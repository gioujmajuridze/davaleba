package com.example.davaleba

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity() {

    private lateinit var editTextEmail:EditText
    private lateinit var editTextPassword:EditText
    private lateinit var editTextPassword2:EditText
    private lateinit var button:Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
        registerListener()

    }

    private fun init() {
        editTextEmail = findViewById(R.id.editTextEmail)
        editTextPassword = findViewById(R.id.editTextPassword)
        editTextPassword2 = findViewById(R.id.editTextPassword2)
        button = findViewById(R.id.button)




    }
    private fun registerListener() {
        button.setOnClickListener {
            val email = editTextEmail.text.toString()
            val password = editTextPassword.text.toString()
            val password2 = editTextPassword2.text.toString()

            if (password!=password2){
                Toast.makeText(this, "შენი შეყვანილი პაროლები არ არის ერთნაირი სცადე თავიდან", Toast.LENGTH_SHORT).show()
            }

            if (email.isEmpty()){
                Toast.makeText(this, "ემაილის ველი არის ცარიელი, შეავსეთ", Toast.LENGTH_SHORT).show()
            }
            if (password==password2 && password.length>=8 && password2.length>=8){
                Toast.makeText(this, "შენ შეიყვანე პაროლი სწორად", Toast.LENGTH_SHORT).show()

                FirebaseAuth.getInstance()
                    .createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener { task ->
                        if(task.isSuccessful){
                            Toast.makeText(this, "გილოცავთ თქვენ დარეგისტრირდით", Toast.LENGTH_SHORT).show()
                        }else{
                            Toast.makeText(this, "რაღაც შეცდომა მოხდა", Toast.LENGTH_SHORT).show()

                        }
                    }

            }else{
                Toast.makeText(this, "შეიყვანეთ რვა სიმბოლოზე მეტი თუ გინდათ დარეგისტრირება", Toast.LENGTH_SHORT).show()
            }


            if(email.isEmpty() || password.isEmpty() || password2.isEmpty()) {
                Toast.makeText(this, "რომელიღაცა ველი არის ცარიელი შეავსეთ ყველა!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener

        }


        }




    }




}